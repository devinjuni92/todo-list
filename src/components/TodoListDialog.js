import { Modal, Button, Form } from 'react-bootstrap';
import { useState, useEffect } from 'react'

const TodoListDialog = ({ handleClose, show, todoList }) => {
  const [todoListItem, setTodoListItem] = useState('');

  useEffect(() => {
    setTodoListItem(todoList.name);
  }, [todoList.name]);
 
  const handleTodoList = (event) => {
    event.preventDefault();
    setTodoListItem(event.target.value)
  }

  return (
    <>
      <Modal 
        show={show} 
        onHide={handleClose}
        >
        <Modal.Header 
          closeButton
          >
          <Modal.Title
            >
            Edit Todo List
          </Modal.Title>
        </Modal.Header>
        <Modal.Body
          >
          <Form.Label
            >
            Todo List
          </Form.Label>
          <Form.Control
            placeholder='Please Create Todo List'
            value={todoListItem ?? ""}
            onChange={handleTodoList}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button 
            variant="primary" 
            onClick={handleClose}
            >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default TodoListDialog