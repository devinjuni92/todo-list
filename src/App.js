import './App.css';
import Todo from './view/Todo';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)

function App() {
  return (
    <div>
      <Todo></Todo>
    </div>
  );
}

export default App;
