import React, { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button,Table, InputGroup, FormControl, Container, Row, Col } from 'react-bootstrap';
import TodoListDialog from "../components/TodoListDialog";

class Todo extends Component{
	constructor() {
    super();
    this.state = {
      show: false,
      todoListData : JSON.parse(localStorage.getItem('todo-list')),
      selectedTodoList : {},
      todoList : ''
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }
  
  showModal = async (index) => {
    await this.setState({
      selectedTodoList : this.state.todoListData[index],
      show : true,
    });
  };
  
  hideModal = () => {
    this.setState({ show: false });
  };

  deleteTodoList = (index) =>{
    let data = this.state.todoListData;
    data.splice(index, 1);

    this.setState({
      todoListData : data,
    })

    localStorage.setItem('todo-list', JSON.stringify(this.state.todoListData));
  }

  addTodoList = (event) => {
    event.preventDefault();
    let todoLists = this.state.todoListData;

    todoLists.push({
      name : this.state.todoList,
      complete : false
    })

    this.setState({
      todoListData : todoLists,
      todoList : ''
    })

    localStorage.setItem('todo-list', JSON.stringify(this.state.todoListData));
  }

  setTodoList = (event) => {
    event.preventDefault();
    this.setState({
      todoList : event.target.value
    })
  }

  complete = (index) => {
    let todoListData = this.state.todoListData[index];
    todoListData.complete = true;
    todoListData = this.state.todoListData;

    this.setState({
      todoListData : todoListData
    })
  }

  showTodoList = () => {
    return this.state.todoListData.map((todoList, index) => {
      return <tr
        key={index}
        >
        <td>{ index + 1 }</td>
        <td>{ todoList.name } {todoList.complete ? <FontAwesomeIcon icon="fa-solid fa-check" /> : ''}</td>
        <td><Button variant="success" onClick={() => this.complete(index)}>Complete</Button></td>
        <td><Button variant="secondary" onClick={() =>this.showModal(index)}>Edit</Button></td>
        <td><Button variant="danger" onClick={() => this.deleteTodoList(index)}><FontAwesomeIcon icon="fa-solid fa-trash" /></Button></td>
      </tr>
    })
  }

  save = () => {
    console.log('save')
  }


	render(){
		return (
			<div 
				className='content'
				>
				<Container>
					<Row 
						className="justify-content-md-center"
						>
						<Col 
							xs 
							lg="4"
							>
							<InputGroup 
								className="mb-3"
								>
								<FormControl
									placeholder="Create Todo List"
									aria-label="Create Todo List"
									aria-describedby="basic-addon2"
                  value={this.state.todoList}
                  onChange={this.setTodoList}
									/>
								<Button 
									variant="primary" 
									id="button-addon2"
                  onClick={this.addTodoList}
									>
									Save
								</Button>
							</InputGroup>
						</Col>
					</Row>
					<Row
						className="justify-content-md-center"
						>
						<Col>
							<Table 
								striped 
								bordered 
								hover
								>
								<thead
									>
									<tr>
										<th>#</th>
										<th>Todo List Name</th>
										<th colSpan={3}>Action</th>
									</tr>
								</thead>
								<tbody>
                  { this.showTodoList() }
								</tbody>
							</Table>
						</Col>
					</Row>
          <TodoListDialog
            show={this.state.show}
            handleClose={this.hideModal}
            todoList={this.state.selectedTodoList}
            >
          </TodoListDialog>
				</Container>
			</div>
	
		);
	}
}

export default Todo;